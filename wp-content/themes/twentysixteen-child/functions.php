<?php

function theme_enqueue_styles() {
	
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );



function tsc_google_analytics() { ?>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-XXXXX-Y', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
	
<?php } 	
add_action( 'wp_footer', 'tsc_google_analytics' );
	
	
	
	
//Adding Function for Custom Text	

/**
 * Name 
 * 
 * Short Description
 * 
 * @param none
 * @return string 
 */
function tsc_custom_text() { ?>
	
<div class="footer_info"><p style=color:white;float:right;padding-right:20px;>Designed & Developed by Paul Myers</p></div>
	
<?php }

add_action( 'wp_footer', 'tsc_custom_text' );




//Adding Function for Google fonts
function tsc_add_gfont() { ?>
	
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Ubuntu::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>
	
<?php }

add_action( 'wp_head', 'tsc_add_gfont' );



remove_action( 'wp_head', 'print_emoji_detection_script', 7);
remove_action( 'wp_head', '_wp_render_title_tag', 1);
remove_action( 'wp_head', 'wp_oembed_add_host_js', 10);
remove_action( 'wp_head', 'wp_post_preview_js', 1);
remove_action( 'wp_head', 'feed_links', 2);
remove_action( 'wp_head', 'feed_links_extra', 3);
remove_action( 'wp_head', 'wp_print_styles', 8);
remove_action( 'wp_head', 'rest_output_link_wp_head', 10);
remove_action( 'wp_head', 'tsc_add_gfont', 10);


/**
 * Name : tsc_content_add
 * 
 * Adding text to the end of the content
 * 
 * @param none
 * @return the content 
 */

function tsc_content_add( $content ) {

		$filteredcontent = $content;

		if ( is_home() ) :
			$filteredcontent .= "hello all";
		endif; 	
		
		return $filteredcontent;

}

add_filter( 'the_content', 'tsc_content_add' );


function tsc_trim_title($title) {
	
	$trim = wp_trim_words( $title, 2, '...');
	
	return $trim;
	
}

add_filter( 'the_title', 'tsc_trim_title' );





function wp_custom_excerpt_length( $thiscouldbecalledanything ) {
	
	return 5;
	
}

add_filter( 'excerpt_length', 'wp_custom_excerpt_length' );

