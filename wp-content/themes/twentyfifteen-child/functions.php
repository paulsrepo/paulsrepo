<?php
//ENQUEUE THE PARENT CSS SCRIPT
	function theme_enqueue_styles() {
		
		wp_enqueue_style( 'parent_style', get_template_directory_uri() . '/style.css' );
		
	}
	
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
	
	
//ADDING GOOGLE ANALYTICS
	function pm_analytics() { ?>
		
		<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		ga('create', 'UA-XXXXX-Y', 'auto');
		ga('send', 'pageview');
	</script>
		<!-- End Google Analytics -->

		
<?php 	}
	
	add_action( 'wp_footer', 'pm_analytics' );
	
	
//ADDING SOME CUSTOM TEXT
	function pm_add_text() { ?>
		
		 <div class="foot_text">
			 
			 <p style="float:right;color: black;">Just a line of text</p>
			 
		 </div>
		 
		
<?php 	}
	
	add_action ( 'wp_footer', 'pm_add_text' );
	
	
//ADDING GOOGLE FONTS
	function pm_google_font() { ?>
		
		<script type="text/javascript">
			WebFontConfig = {
			google: { families: [ 'Indie+Flower::latin' ] }
			};
			(function() {
			var wf = document.createElement('script');
			wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
			})(); 
		</script>
		
<?php	}
	
	add_action( 'wp_head', 'pm_google_font' );
	
	
//REMOVE ACTION
	remove_Action( 'wp_head', 'print_emoji_detection_script', 7 ); 
	
	
//FILTER
	function pm_post_name($title) {
		
		if (is_home() || is_front_page()) :
		
			$title = wp_trim_words( $title, $num_words=2, $more ='..');
		
		endif;
		
		return $title;
		
		
	}
	
	add_filter( 'the_title', 'pm_post_name' );
	
	
//CUSTOM FUNCTION	
	function pm_site_info() {
		
		do_action( 'pm_site_info' );
		
	}
	
//CONDITIONALS
	function pm_conditional() {
		
		$markup_start = '<div class="page-identifier">';
		$markup = '</div>';
		
		if ( is_home() || is_front_page() ) :
			
			echo sprintf( '%1s%2s%3s', $markup_start, "This is the homepage", $markup );
		
		elseif ( is_single() ):
			
			$variable = sprintf('<div class="page-identifier">%s</div>', "This is just a Single Page");
			
			echo $variable;
			
		elseif ( is_post() ):
			
			printf(
			'<div class="page-identifier">%s</div>', 
				"This is only a Post") ;
			
		elseif ( is_page() ):
			
			echo "This is a Page";
			
		else:
			
			echo "This is something else";
			
		endif;
		
	}
	
	add_action( 'pm_site_info', 'pm_conditional', 10 );
	
	
//ADDITION OF TEXT ABOVE THE CONDITIONAL
	function pm_additional_text() {
		
		echo sprintf( '<div class="site-info-title"><h1>%s</h1></div>', 'What page is this?' );
		
	}
	
	add_action( 'pm_site_info', 'pm_additional_text', 5 );
	
	
	
	
	
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/MetroNet.svg);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
	
	
	