
var gulp = require( 'gulp' ); // Requires the GULP plugin

var sass = require( 'gulp-sass' ); // Required the GULP Sass Plugin

gulp.task( 'sass', function() {
	
	return gulp.src( 'sass/style.scss' ) // Providing the source files
	
	.pipe(sass()) // Using GULP Sass to convert Sass to CSS
	.pipe (gulp.dest( '.' )) // Destination of the CSS file
	
});